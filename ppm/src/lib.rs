use std::fs::File;
use std::io::prelude::*;
use std::io::BufWriter;

pub struct Ppm {
    writer: BufWriter<File>,
    format: String,
    dims: (usize, usize),
    buf: Vec<Vec<(u8, u8, u8)>>,
}

impl Ppm {
    pub fn new(path: &str, (width, height): (usize, usize)) -> Self {
        let file = File::create(path).expect("Failed to create foo.ppm");

        // Raw PPM is up to 70 characters per line. Just allocate that.
        let writer = BufWriter::with_capacity(70 * (width * height + 3), file);

        // See: https://github.com/rust-lang/rust/issues/54628
        let buf = vec![vec![(0, 0, 0); width]; height];

        Ppm {
            writer,
            format: "P3".to_string(),
            dims: (width, height),
            buf,
        }
    }

    pub fn set_type(&mut self, s: &str) {
        self.format = s.to_string()
    }

    pub fn set_dims(&mut self, dims: (usize, usize)) {
        self.dims = dims
    }

    pub fn set_pixel(&mut self, loc: (usize, usize), rgb: (u8, u8, u8)) {
        self.buf[self.dims.1 - 1 - loc.1][loc.0] = rgb
    }

    pub fn get_pixel(&mut self, loc: (usize, usize)) -> (u8, u8, u8) {
        self.buf[self.dims.1 - 1 - loc.1][loc.0]
    }

    pub fn sync(&mut self) -> std::io::Result<()> {
        self.writer.write_fmt(format_args!("{}\n", self.format))?;
        self.writer
            .write_fmt(format_args!("{} {}\n", self.dims.0, self.dims.1))?;
        self.writer.write_fmt(format_args!("{}\n", 255))?;
        for i in self.buf.iter().flatten() {
            self.writer
                .write_fmt(format_args!("{} {} {}\n", i.0, i.1, i.2))?;
        }
        Ok(())
    }
}
