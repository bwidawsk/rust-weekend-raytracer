extern crate ppm;
extern crate raytrace;

use raytrace::trace;

fn main() {
    let nx = 200;
    let ny = 100;
    let mut ppm = ppm::Ppm::new("gradient.ppm", (nx, ny));
    let tracer = trace::gradient_tracer(nx, ny);
    for j in 0..ny {
        for i in 0..nx {
            let res = tracer(i, j);
            ppm.set_pixel((i, j), (res.r, res.g, res.b));
        }
    }
    ppm.sync();
}
