/* trace.rs is public domain */

use crate::ray;
use crate::vec3::Vec3;

pub struct PixelResult {
    pub x: usize,
    pub y: usize,
    pub r: u8,
    pub g: u8,
    pub b: u8,
}

type RayTracer = dyn Fn(usize, usize) -> PixelResult;

pub fn gradient_tracer(w: usize, h: usize) -> Box<RayTracer> {
    let ratio = (w as f64 / h as f64) as f32;
    let lower_left = Vec3 {
        x: -ratio,
        y: -1.0,
        z: -1.0,
    };
    let horizontal = Vec3 {
        x: 2f32 * ratio,
        y: 0.0,
        z: 0.0,
    };
    let vertical = Vec3 {
        x: 0.0,
        y: 2.0,
        z: 0.0,
    };
    let origin = Vec3 {
        x: 0.0,
        y: 0.0,
        z: 0.0,
    };
    /* C0 is white, C1 is blueish */
    let c0 = Vec3 {
        x: 1.0,
        y: 1.0,
        z: 1.0,
    };
    let c1 = Vec3 {
        x: 0.5,
        y: 0.7,
        z: 1.0
    };
    Box::new(move |x, y| {
        let u: f32 = x as f32 / w as f32;
        let v: f32 = y as f32 / h as f32;
        let dir = lower_left + horizontal * u + vertical * v;
        let r = ray::Ray::new(&origin, &dir, &c0, &c1);
        let col = ray::color(r);
        let r: u8 = (col.x * 255.99f32) as u8;
        let g: u8 = (col.y * 255.99f32) as u8;
        let b: u8 = (col.z * 255.99f32) as u8;
        PixelResult { x, y, r, g, b }
    })
}
