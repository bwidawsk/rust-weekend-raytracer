#[derive(Debug, Clone, Copy)]
pub struct Vec3 {
    pub x: f32,
    pub y: f32,
    pub z: f32,
}

pub fn dot(u: &Vec<f32>, v: &Vec<f32>) -> f32 {
    assert_eq!(u.len(), v.len());
    u.iter().zip(v.iter()).map(|(x, y)| x * y).sum()
}

pub fn normalize(v: &Vec3) -> Vec3 {
    //  w = v * rsq(v.dot(v)) reciprocal-square root v·v
    let x = vec![v.x, v.y, v.z];
    *v * (1.0 / (dot(&x, &x)).sqrt())
}

impl Vec3 {
    #[allow(dead_code)]
    fn length(&self) -> f32 {
        (self.x.powi(2) + self.y.powi(2) + self.z.powi(2)).sqrt()
    }
}

use std::ops::{Add, AddAssign, Mul};
impl Add for Vec3 {
    type Output = Vec3;

    fn add(self, other: Vec3) -> Vec3 {
        Vec3 {
            x: self.x + other.x,
            y: self.y + other.y,
            z: self.z + other.z,
        }
    }
}

impl AddAssign for Vec3 {
    fn add_assign(&mut self, other: Self) {
        *self = Self {
            x: self.x + other.x,
            y: self.y + other.y,
            z: self.z + other.z,
        };
    }
}

impl Mul<f32> for Vec3 {
    type Output = Self;

    fn mul(self, scalar: f32) -> Self::Output {
        Vec3 {
            x: self.x * scalar,
            y: self.y * scalar,
            z: self.z * scalar,
        }
    }
}

#[cfg(test)]
mod tests {
    use crate::vec3::*;
    #[test]
    fn unit_dot() {
        /* Test some basic maths */
        let unit = vec![1.0, 0.0, 0.0];
        let dot = dot(&unit, &unit);
        assert_eq!(dot, 1.0f32);
    }
    #[test]
    fn orth_dot() {
        /* Test some basic maths */
        let horiz = vec![1.0, 0.0, 0.0];
        let vert = vec![0.0, 1.0, 0.0];
        assert_eq!(dot(&horiz, &vert), 0.0f32);
    }

    #[test]
    fn null_dot() {
        let vec = vec![1.0, 1.0, 1.0];
        assert_eq!(dot(&vec, &vec![0.0, 0.0, 0.0]), 0.0f32);
    }

}
