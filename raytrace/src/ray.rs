use crate::vec3;
use crate::vec3::Vec3;

pub fn color(r: Ray) -> Vec3 {
    let dir = r.direction();
    let unit_dir = vec3::normalize(dir);
    let t: f32 = (unit_dir.y + 1.0) * 0.5;
    r.c0 * (1.0 - t) + r.c1 * t
}
/*
 * c0 and c1 are the background colors (vertical gradient if different) and they are used when the
 * ray doesn't hit an object.
 */
pub struct Ray {
    origin: Vec3,
    direction: Vec3,
    c0: Vec3,
    c1: Vec3
}

impl Ray {
    pub fn new(a: &Vec3, b: &Vec3, c0: &Vec3, c1: &Vec3) -> Self {
        Ray {
            origin: *a,
            direction: *b,
            c0: *c0,
            c1: *c1,
        }
    }
    #[allow(dead_code)]
    pub fn origin(&self) -> &Vec3 {
        &self.origin
    }
    pub fn direction(&self) -> &Vec3 {
        &self.direction
    }
    #[allow(dead_code)]
    fn point_at_parameter(&self, t: f32) -> Vec3 {
        self.origin + self.direction * t
    }
}
